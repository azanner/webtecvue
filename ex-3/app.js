const app = Vue.createApp({
  data() {
    return {
      likes: 0,
      money: 0,
      comment: {
        author: "Aljoscha",
        text: "Test eins zwo drei",
      },
    };
  },
  methods: {
    donate(val) {
      this.money += val;
    },
    saveComment(e) {
      this.comment.text = e.target[0].value;
      this.comment.author = e.target[1].value;
    },
  },
});

app.mount("#app");
