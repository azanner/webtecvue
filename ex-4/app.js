const app = Vue.createApp({
  data() {
    return {
      likes: 0,
      money: 0,
      alert: false,
      comment: {
        author: "Zanner",
        authorFirstName: "Aljoscha",
        text: "Test eins zwo drei",
      },
    };
  },
  methods: {
    donate(val) {
      this.money += val;
    },
    saveComment(e) {
      this.comment.text = e.target[0].value;
      this.comment.author = e.target[1].value;
      // könnte natürlich auch hier alert auf true setzen
    },
  },
  watch: {
    "comment.author"(newVal, oldVal) {
      console.log("old", oldVal);
      if (newVal == "bleymehl" || newVal == "Bleymehl") {
        this.alert = true;
      }
    },
  },
});

app.mount("#app");
