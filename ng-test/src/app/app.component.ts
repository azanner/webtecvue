import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'ng-test';
  UweGewichtPlus: number = 0;
  RolfGewichtPlus: number = 0;
  Ausgangsgewicht: number = 70;
  uweLebt: boolean = true;

  constructor() {}

  onClickAddUwe(): void {
    this.UweGewichtPlus++;
  }
  onClickAddRolf(): void {
    this.RolfGewichtPlus++;
  }
  gewichtUwe(): number {
    console.log('uwe nimmt zu');
    return this.Ausgangsgewicht + this.UweGewichtPlus;
  }
  gewichtRolf(): number {
    console.log('Rolf nimmt zu');
    return this.Ausgangsgewicht + this.RolfGewichtPlus;
  }
}
