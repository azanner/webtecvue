const app = Vue.createApp({
  data() {
    return {
      likes: 0,
      money: 0,
    };
  },
  methods: {
    donate(val) {
      this.money += val;
    },
  },
});

app.mount("#app");
