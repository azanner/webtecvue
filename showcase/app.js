const app = Vue.createApp({
  data() {
    return {
      name: "Aljoscha",
      active: true,
      userInput: "value",
      checked: true,
      userLang: "de",
      fruits: [
        { id: 0, item: "Banana" },
        { id: 0, item: "Apple" },
        { id: 0, item: "Lemon" },
        { id: 0, item: "Orange" },
        { id: 0, item: "Grapes" },
      ],
      UweGewichtPlus: 0,
      RolfGewichtPlus: 0,
      Ausgangsgewicht: 70,
      uweLebt: true,
    };
  },
  computed: {
    gewichtUwe() {
      console.log("uwe nimmt zu");
      return this.Ausgangsgewicht + this.UweGewichtPlus;
    },
    gewichtRolf() {
      console.log("Rolf nimmt zu");
      return this.Ausgangsgewicht + this.RolfGewichtPlus;
    },
  },
  methods: {
    makeActive(isActive) {
      this.active = isActive;
    },
    updateUserInput(event) {
      console.log(event);
    },
    handleSubmit(event) {
      console.log(event);
    },
  },
  created() {
    console.log("Hallo aus dem created Hook");
  },
  watch: {
    UweGewichtPlus(newValue, oldValue) {
      console.log("Hallo aus dem Weight Watcher");
      console.log("newValue", newValue);
      console.log("oldValue", oldValue);
      if (this.UweGewichtPlus == 20) {
        this.uweLebt = false;
      }
    },
  },
});

app.mount("#app");
