# How to enjoy the Vue:

## Table of Contents:

- [Exercise Docs](#exercise-docs)
  - [Interpollation](#interpollation)
  - [Binding Attributes](#binding-attributes)
  - [Styling](#styling)
  - [Binding Events](#binding-attributes)
  - [Methods](#methods)
  - [Forms](#forms)
  - [Alternativen zu Methods](#alternativen-zu-methods)
    - [Computed](#computed)
    - [Watch](#watch)
  - [Conditional Rendering](#conditional-rendering)
  - [List Rendering](#list-rendering)
- [Nice to Haves](#nice-to-haves)
  - [Setup](#setup)
  - [VS-Extensions](#vs-extensions)
  - [Links](#links)

&nbsp;

---

&nbsp;

## Exercise Docs

Vue Syntax fängt mit `v-` an

Bsp:

```
v-bind
v-on
v-for
v-if
v-model
```

&nbsp;

---

&nbsp;

### Interpollation

Ähnlich wie bei Angular kann Code in das HTML Dokument gerendert werden:

Bsp:

```
{{variable}}
{{ test ? 'richtig' : 'falsch'}}
```

&nbsp;

---

&nbsp;

### Binding Attributes

HTML Attribute können dynamisch verändert werden. Hierzu muss man Vue an das Attribute mit `v-bind` "binden".

#### Bsp:

```
v-bind:class=”{active: true}”
Shortcut:
:class=”{active: true}”
```

&nbsp;

---

&nbsp;

### Styling:

Binding kann dazu genutzt werden elemente dynamisch zu stylen:

Bsp:

```
Option A Inline styling :style=”borderColor: ’red’”
Option B Dynamic classes :class=”active: true”
```

&nbsp;

---

&nbsp;

### Binding Events

Es ist möglich auf native oder eigens erstelle Events zu reagieren. Hierzu verwendet man die `v-on` Syntax:

Bsp:

```
v-on:click=”doSomething”
Shortcut:
@click=”doSomething”

```

Events können durch modifier angepasst werden.

Bsp: "Tastendruck.nurEntertaste"

```
@keydown.enter="submitSomething"

```

Docs: https://vuejs.org/v2/guide/events.html

&nbsp;

---

&nbsp;

### Methods

Methoden können in der Vue Instance definiert werden:

```
const app = Vue.createApp({
  data() {
    return {

    }
  },
  methods: {
    doSomething() {

      // execute some Code
    }
  }
})

app.mount('#app')
```

und sowohl inderhalb der Vue Instance als auch im HTML zum Einsatz kommen:

```
{{increaseCounter()}}
@click=”increaseCounter”
```

Docs: https://vuejs.org/v2/guide/events.html#Method-Event-Handlers

&nbsp;

---

&nbsp;

### Forms

Auch auf das Submit-Event des Form-Elementes kann reagiert werden:

```
<form @submit.prevent=”sendSomeData”>
```

&nbsp;

---

&nbsp;

#### Two Way Data Binding

Für Userinput gut es `v-model`. Es nimmt einem die Arbeit ab, selbst auf die Inputevents reagieren zu müssen.

```
<input v-model="message" placeholder="edit me">
<p>Message is: {{ message }}</p>
```

Vue Docs: https://vuejs.org/v2/guide/forms.html

&nbsp;

---

&nbsp;

### Alternativen zu Methods

#### Computed:

Computed Functions können in der Vue Instance definiert werden:
Sie werden nur ausgeführt, wenn sich Werte ändern, die sie betreffen.

```
const app = Vue.createApp({
  data() {
    return {

    }
  },
  computed: {
    doSomething() {
      return returnSomething
      // wichtig: computed functions brauchen einen return wert!!
    }
  }
})

app.mount('#app')
```

#### Watch:

Watcher können in der Vue Instance definiert werden:
Watcher "beobachten" ein Datenobjekt und werden ausgeführt sobald sich dieses ändert.

```
const app = Vue.createApp({
  data() {
    return {
      name: 'Aljoscha'
    }
  },
  watch: {
    name() {
      execute some Code
      // wichtig: die function muss den Namen von der Propertie tragen, die beobachtet werden soll
    }
  }
})

app.mount('#app')
```

&nbsp;

---

&nbsp;

### Conditional Rendering

Im HTMl template können Elemente auch nur gerendert werden, wenn gewisse Bedingungen erfüllt sind:

```

<div v-if="someTruthyValue">
show me
</div>

<div v-else-if="someOtherTruthyValue">
show me as well
</div>

<div v-else>
or show me
</div>

// simplere alternative

<div v-show="someTruthyValue">
show me
</div>

```

&nbsp;

---

&nbsp;

### List Rendering

Auch for Schleifen können ausgeführt werden

```

<li
  v-for=”item in items”
  :key=”item.id”
  >
  {{item.title}}
</li>

```

&nbsp;

---

&nbsp;

## Setup

Um mal reinzuschnuppern reicht ein CDN Link zu der aktuellesten Version. Dieser kann einfach im HTML Dokument importiert werden.

```
Vue2:
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
Vue3:
<script src="https://unpkg.com/vue@next" defer></script>
```

Vue CLI:

Um über das Terminal Vue Applikationen erstellen zu können und beispielsweise plugins zu intallieren kann das Vue Command Line Interface eingesetzt werden.

Installation :

```
npm install -g @vue/cli
```

Neues Projekt erstellen :

```
vue create beispiel-name
```

Plugin Installieren :

```
vue add quasar
```

Alles weitere findet ihr hier: https://vuejs.org/v2/guide/installation.html

&nbsp;

---

&nbsp;

## VS-Extensions:

- Vue VSCode Snippets (sahra drasner)
- Vetur

&nbsp;

---

&nbsp;

## Links

- https://github.com/vuejs/awesome-vue
- https://vuejsdevelopers.com/
- https://www.vuemastery.com/
- https://frontendmasters.com/learn/vue/
- https://vueschool.io/articles/
- https://quasar.dev
- https://css-tricks.com/native-like-animations-for-page-transitions-on-the-web/
